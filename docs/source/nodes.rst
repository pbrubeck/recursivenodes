nodes
=====

.. default-role:: math

.. testsetup:: *

   from recursivenodes.nodes import equispaced, equispaced_interior, blyth_luo_pozrikidis, warburton, warburton_alpha


All of the node sets for the `d`-simplex implemented by ``recursivenodes``.

.. toctree::
   :maxdepth: 2

.. module:: recursivenodes.nodes

Node sets on the simplex
------------------------

.. function:: recursive(d, n, family='lgl', domain='barycentric')

   Alias of :func:`recursivenodes.recursive_nodes`.

.. autofunction:: equispaced

.. autofunction:: equispaced_interior

.. autofunction:: blyth_luo_pozrikidis

.. autofunction:: warburton

.. autodata:: warburton_alpha
   :annotation:

.. autofunction:: rapetti_sommariva_vianello

.. _families:

The ``family`` optional argument
--------------------------------

A full definition of 1D node families is given in the documentation of
:func:`recursivenodes.recursive_nodes`.

Here are ways to specify 1D node families in ``recursivenodes``:

- Shifted Lobatto-Gauss-Jacobi nodes for different values of the weight exponents `\boldsymbol{\alpha}` and `\beta`,
  which always include the endpoints `0` and `1`:

  - ``'lgl'`` (*str*) -- Shifted L-G-*Legendre* (`\boldsymbol{\alpha} = \beta = 0`).
  - ``'lgc'`` (*str*) -- Shifted L-G-*Chebyshev* (`\boldsymbol{\alpha} = \beta = -1/2`).
  - ``('lgg', a)`` ((*str*, *float*)) -- Shifted L-G-*Gegenbauer* (`\boldsymbol{\alpha} = \beta = a - 1/2`).

- Shifted Gauss-Jacobi nodes for different values of the weight exponents `\boldsymbol{\alpha}` and `\beta`,
  which are in the interior away from the endpoints `0` and `1`:

  - ``'gl'`` (*str*) -- Shifted G-*Legendre* (`\boldsymbol{\alpha} = \beta = 0`).
  - ``'gc'`` (*str*) -- Shifted G-*Chebyshev* (`\boldsymbol{\alpha} = \beta = -1/2`).
  - ``('gg', a)`` ((*str*, *float*)) -- Shifted G-*Gegenbauer* (`\boldsymbol{\alpha} = \beta = a - 1/2`).

- Equispaced nodes:

  - ``'equi'`` (*str*) -- Equispaced nodes including the endpoints `0` and `1`.
  - ``'equi_interior'`` (*str*) -- Equispaced nodes excluding the endpoints `0`
    and `1`.  The `k`-point equispaced interior nodes are a half-phase off from
    the `(k+1)`-point equispaced nodes.

    .. doctest::

       >>> equispaced(1, 4, domain='unit')
       array([[0.  ],
              [0.25],
              [0.5 ],
              [0.75],
              [1.  ]])
       >>> equispaced_interior(1, 3, domain='unit')
       array([[0.125],
              [0.375],
              [0.625],
              [0.875]])

.. seealso::

   :func:`recursivenodes.quadrature.lobattogaussjacobi`
   :func:`recursivenodes.quadrature.gaussjacobi`

.. _domains:

The ``domain`` optional argument
--------------------------------

Four reference domains for the `d`-simplex are implemented in ``recursivenodes``.

- ``'unit'`` (*str*) -- In `\mathbb{R}^d`, defined by `x_i \geq 0, \sum_i x_i <= 1`.

- ``'biunit'`` (*str*) -- In `\mathbb{R}^d`, defined by `x_i \geq -1, \sum_i x_i <= 2-d`.

- ``'equilateral'`` (*str*) -- In `\mathbb{R}^d`, defined by having:

  - edge length 2,
  - centroid at the origin,
  - a downward-facing facet,
  - a projection onto the first `d-1` coordinates that is the corresponding equilateral `d-1`-simplex.

  (For `d=1`, ``'equilateral'`` is the same as ``'biunit'``).

- ``'barycentric'`` (*str*) -- In `\mathbb{R}^{d+1}`, defined by  `x_i \geq 0, \sum_i x_i = 1``.

  .. plot::

     >>> import matplotlib.pyplot as plt
     >>> from matplotlib.collections import PatchCollection, PolyCollection
     >>> from mpl_toolkits.mplot3d import Axes3D
     >>> from recursivenodes.nodes import equispaced
     >>> plt.figure()
     >>> for (domain, loc) in zip(['unit', 'biunit', 'equilateral'],[221,222,223]):
     >>>     ax = plt.subplot(loc)
     >>>     vertices = equispaced(2, 1, domain=domain)
     >>>     polygon = plt.Polygon(vertices, True)
     >>>     ax.add_collection(PatchCollection([polygon]))
     >>>     ax.set_aspect('equal')
     >>>     ax.set_title(domain)
     >>>     ax.axhline(c='grey')
     >>>     ax.axvline(c='grey')
     >>>     ax.autoscale(True)
     >>> ax = plt.subplot(224, projection='3d')
     >>> vertices = equispaced(2, 1, domain='barycentric')
     >>> ax.plot_trisurf(vertices[:,0], vertices[:,1], vertices[:,2])
     >>> ax.view_init(None, 45)
     >>> ax.set_title('barycentric')
     >>> ax.autoscale(True)
     >>> plt.tight_layout()
     >>> plt.show()

.. seealso::

   :func:`recursivenodes.utils.coord_map`
