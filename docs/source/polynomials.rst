polynomials
===========

.. default-role:: math

.. toctree::
   :maxdepth: 2

.. automodule:: recursivenodes.polynomials
   :members:

