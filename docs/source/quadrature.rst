quadrature
==========

.. default-role:: math

.. toctree::
   :maxdepth: 2

.. automodule:: recursivenodes.quadrature
   :members:

