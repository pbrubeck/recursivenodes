metrics
=======

.. default-role:: math

.. toctree::
   :maxdepth: 2

.. automodule:: recursivenodes.metrics
   :members:


