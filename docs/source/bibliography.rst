------------
Bibliography
------------

.. toctree::
   :maxdepth: 2

.. bibliography:: recursivenodes.bib
   :all:
