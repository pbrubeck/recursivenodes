===================================================================================================
``recursivenodes:`` Recursive, parameter-free, explicitly defined interpolation nodes for simplices
===================================================================================================

.. default-role:: math

.. testsetup:: *

   from recursivenodes import recursive_nodes

.. plot::

   import matplotlib.pyplot as plt
   from mpl_toolkits.mplot3d import Axes3D
   from recursivenodes import recursive_nodes

   nodes1 = recursive_nodes(3, 1, domain='equilateral')
   nodes3 = recursive_nodes(3, 3, domain='equilateral')
   nodes7 = recursive_nodes(3, 7, domain='equilateral')
   plt.figure(figsize=(12,4))
   ax = plt.gca(projection='3d')
   for (i,nodes) in enumerate([nodes1, nodes3, nodes7]):
     ax.scatter(nodes[:,0]+3.*i, nodes[:,1]+1.*i, nodes[:,2])
   ax.set_proj_type('ortho')
   ax.axis('off')
   ax.auto_scale_xyz([0, 5], [0, 3], [-0.3, 0.8])
   ax.dist = 9.5
   plt.tight_layout()
   plt.show()

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   bibliography
   genindex

-------------
What is this?
-------------
   
.. module:: recursivenodes

.. autofunction:: recursive_nodes 

-------------------
Why does it matter?
-------------------

`Runge's phenomenon`_: when constructing a polynomial interpolant `p_f^n \in
\mathcal{P}_n(\Delta^d)` that interpolates `f` at given points
`X_n = \{\boldsymbol{x}_{n,i}\}` ("interpolation nodes"),

.. math::
   p_f^n(\boldsymbol{x}_{n,i}) = f(\boldsymbol{x}_{n,i}), \quad i=1,\dots, \binom{n+d}{d},

the locations of the `\boldsymbol{x}_{n,i}`'s has a big effect on how different `p_f^n` is from
`f` on `\Delta^d`.  Equispaced nodes can define bad interpolants, most famously for the `Witch of Agnesi`_:

.. _Runge's phenomenon: https://en.wikipedia.org/wiki/Runge%27s_phenomenon

.. plot::

   import numpy as np
   import matplotlib.pyplot as plt
   from mpl_toolkits.mplot3d import Axes3D
   from recursivenodes.nodes import equispaced
   grid = equispaced(2, 63, 'equilateral')
   radius = np.linalg.norm(grid, axis=-1)
   f = 1/(1 + 25*radius**2)
   ax = plt.gca(projection='3d')
   ax.plot_trisurf(grid[:,0], grid[:,1], f)
   ax.set_title('"Witch of Agnesi" function')
   ax.axis('off')
   plt.tight_layout()
   plt.show()

Here is the degree 15 equispaced polynomial interpolant:

.. plot::

   import numpy as np
   import matplotlib.pyplot as plt
   from mpl_toolkits.mplot3d import Axes3D
   from recursivenodes.nodes import equispaced
   from recursivenodes.polynomials import proriolkoornwinderdubinervandermonde as pkdv
   from recursivenodes.utils import coord_map
   grid = equispaced(2, 63, 'equilateral')
   nodes = equispaced(2, 15, 'equilateral')
   biunit_nodes = coord_map(nodes, 'equilateral', 'biunit')
   biunit_grid = coord_map(grid, 'equilateral', 'biunit')
   Vnodes = pkdv(2, 15, biunit_nodes)
   Vgrid = pkdv(2, 15, biunit_grid)
   radiusnodes = np.linalg.norm(nodes, axis=-1)
   fnodes = 1/(1 + 25*radiusnodes**2)
   pkdf = np.linalg.solve(Vnodes, fnodes)
   interpolant_grid = Vgrid.dot(pkdf)
   ax = plt.gca(projection='3d')
   ax.plot_trisurf(grid[:,0], grid[:,1], interpolant_grid)
   ax.set_title('"Witch of Agnesi" degree 15 equispaced interpolant')
   ax.axis('off')
   plt.tight_layout()
   plt.show()

Now here is the degree 15 interpolant constructed from :func:`recursive_nodes`:

.. plot::

   import numpy as np
   import matplotlib.pyplot as plt
   from mpl_toolkits.mplot3d import Axes3D
   from recursivenodes.nodes import equispaced
   from recursivenodes import recursive_nodes
   from recursivenodes.polynomials import proriolkoornwinderdubinervandermonde as pkdv
   from recursivenodes.utils import coord_map
   grid = equispaced(2, 63, 'equilateral')
   nodes = recursive_nodes(2, 15, domain='equilateral')
   biunit_nodes = coord_map(nodes, 'equilateral', 'biunit')
   biunit_grid = coord_map(grid, 'equilateral', 'biunit')
   Vnodes = pkdv(2, 15, biunit_nodes)
   Vgrid = pkdv(2, 15, biunit_grid)
   radiusnodes = np.linalg.norm(nodes, axis=-1)
   fnodes = 1/(1 + 25*radiusnodes**2)
   pkdf = np.linalg.solve(Vnodes, fnodes)
   interpolant_grid = Vgrid.dot(pkdf)
   ax = plt.gca(projection='3d')
   ax.plot_trisurf(grid[:,0], grid[:,1], interpolant_grid)
   ax.set_title('"Witch of Agnesi" degree 15 recursive interpolant')
   ax.axis('off')
   plt.tight_layout()
   plt.show()

The most accepted way to characterize the interpolation quality of a set of
nodes `X_n` on the `d`-simplex is ":ref:`lebesgueconstant`",
`\Lambda_n^{\max}(X_n):\mathbb{R}^{\binom{n+d}{d}d} \to \mathbb{R}`.

.. _Witch of Agnesi: https://en.wikipedia.org/wiki/Witch_of_Agnesi

-------------------------------------
Are :func:`recursive_nodes` the best?
-------------------------------------

No.

But in some cases they are the best that have yet been computed.

To find the best interpolation nodes requires minimizing `\Lambda_n^{\max}(X_n)`: it is nonconvex and there is no known explicit formula for nodes that minimize it.

2D
==

For the triangle, authors have optimized related functions :cite:`ChBa95,Hest98,Roth05`,
directly optimized `\Lambda_n^{\max}(X_n)` over all potential nodes
:cite:`Roth05,Hein05,RaSV12`, and directly optimized `\Lambda_n^{\max}(X_n)` while 
preserving desirable properties such as symmetry :cite:`Hein05,RaSV12`.

.. plot::

   import matplotlib.pyplot as plt
   from recursivenodes.lebesgue import lebesguemax_reference

   groups = [
       ('recursive', 'recursive_nodes', (4, 16, 1)),
       ('Lebesgue', '[Rot05]', (4, 16, 1)),
       ('Rapetti-Sommariva-Vianello-leb', '[RSV12]', (4, 19, 1)),
       ('Rapetti-Sommariva-Vianello-lebgl', '[RSV12]-LGL', (4, 19, 1)),
       ('Rapetti-Sommariva-Vianello-lebgls', '[RSV12]-LGL-sym', (4, 19, 1)),
       ]
   plt.figure()
   for group in groups:
       key = group[0]
       label = group[1]
       first, last, step = group[2]
       ns = list(range(first, last, step))
       ls = [lebesguemax_reference(2, n, key) for n in ns]
       plt.scatter(ns, ls, label=label)
   plt.title('Lebesgue Constants in 2D')
   plt.legend()
   plt.xlabel('polynomial degree n')
   plt.show()

In this figure, :func:`recursive_nodes` (blue) are on par with the best known node
sets until about `n = 10`, and then perform worse.  Both Roth :cite:`Roth05`
(orange), and Rapetti, Sommariva, and Vianello :cite:`RaSV12` (green) have
calculated node sets that try to minimize the Lebesgue constant directly.
That they disagree for some degrees is an indicator of the difficulty
of finding the global minimum of `\Lambda_n^{\max}(X_n)`.

Rapetti, Sommariva, and Vianello in the same work also computed constrained minima of `\Lambda_n^{\max}(X_n)`, restricting the node sets to:

- node sets whose trace nodes on the edges are the LGL nodes (red), and
- node sets with full `S_3` symmetry whose trace nodes on the edges are the LGL nodes (purple).

In contrast to node sets *implicitly* defined by a minimization problem are
node families with *explicitly* defined construction rules.  Equispaced and
:func:`recursive_nodes` have already been seen: this package also `implements
<./nodes.html>`_ the nodes of Blyth, Luo and Pozrikidis :cite:`BlPo06,LuPo06`
and the widely used *warp & blend* nodes of Warburton :cite:`Warb06` (although
these nodes do have a single tuning parameter that has been optimized
separately for degrees up to 15).

.. plot::

   import matplotlib.pyplot as plt
   from recursivenodes.lebesgue import lebesguemax_reference

   groups = [
       ('recursive', 'recursive_nodes', (4, 16, 1)),
       ('equispaced', 'equispaced', (4, 7, 1)),
       ('blyth_luo_pozrikidis', 'blyth_luo_pozrikidis', (4, 13, 1)),
       ('warburton', 'warburton', (4, 16, 1)),
       ]
   plt.figure()
   for group in groups:
       key = group[0]
       label = group[1]
       first, last, step = group[2]
       ns = list(range(first, last, step))
       ls = [lebesguemax_reference(2, n, key) for n in ns]
       plt.scatter(ns, ls, label=label)
   plt.title('Lebesgue constants in 2D, explicitly defined node families')
   plt.legend()
   plt.xlabel('polynomial degree n')
   plt.show()

Among explicitly defined node families, :func:`recursive_nodes` are usually within
~2% of Warburton's nodes, which perform best.  This is because, despite the different
ways the node families are defined and computed, they are quite similar:

.. plot::
   :include-source: True

   >>> from recursivenodes import recursive_nodes
   >>> from recursivenodes.nodes import warburton
   >>> import matplotlib.pyplot as plt
   >>> nr = recursive_nodes(2, 15, domain='equilateral')
   >>> nw = warburton(2, 15, domain='equilateral')
   >>> ax = plt.gca()
   >>> ax.scatter(nr[:,0], nr[:,1], marker='x', label='recursive_nodes')
   <matplotlib.collections.PathCollection object at ...>
   >>> ax.scatter(nw[:,0], nw[:,1], marker='+', label='warburton')
   <matplotlib.collections.PathCollection object at ...>
   >>> ax.set_aspect('equal')
   >>> ax.legend()
   <matplotlib.legend.Legend object at ...>
   >>> plt.show()

3D
==

In 3D, directly optimizing `\Lambda_n^{\max}(X_n)` becomes more
challenging, because the size of `X_n` grows cubically with `n`.
It seems none of the authors who directly optimized for nodes for the triangle
have yet followed up with tetrahedral nodes.  Only approaches that optimize
simpler, related functions have been extended to the tetrahedron
:cite:`ChBa96,HeTe00`.

.. plot::

   import matplotlib.pyplot as plt
   from recursivenodes.lebesgue import lebesguemax_reference

   groups = [
       ('recursive', 'recursive_nodes', (4, 16, 1)),
       ('Chen-Babuska', '[CB96]', (4, 10, 1)),
       ('Hesthaven-Teng', '[HT00]', (4, 10, 1)),
       ]
   plt.figure()
   for group in groups:
       key = group[0]
       label = group[1]
       first, last, step = group[2]
       ns = list(range(first, last, step))
       ls = [lebesguemax_reference(3, n, key) for n in ns]
       plt.scatter(ns, ls, label=label)
   plt.title('Lebesgue constants in 3D')
   plt.legend()
   plt.xlabel('polynomial degree n')
   plt.show()

In this figure, indirect optimization approaches are not noticeably better or
worse than :func:`recursive_nodes` for the degrees that have been computed.

.. plot::

   import matplotlib.pyplot as plt
   from recursivenodes.lebesgue import lebesguemax_reference

   groups = [
       ('recursive', 'recursive_nodes', (4, 16, 1)),
       ('equispaced', 'equispaced', (4, 12, 1)),
       ('blyth_luo_pozrikidis', 'blyth_luo_pozrikidis', (4, 14, 1)),
       ('warburton', 'warburton', (4, 16, 1)),
       ]
   plt.figure()
   for group in groups:
       key = group[0]
       label = group[1]
       first, last, step = group[2]
       ns = list(range(first, last, step))
       ls = [lebesguemax_reference(3, n, key) for n in ns]
       plt.scatter(ns, ls, label=label)
   plt.title('Lebesgue constants in 3D, explicitly defined node families')
   plt.legend()
   plt.xlabel('polynomial degree n')
   plt.show()

Among explicitly defined node families, :func:`recursive_nodes` are barely worse
than the best for `n < 7` and are best by an increasing margin for larger degrees.

-----------------------------------
So why use :func:`recursive_nodes`?
-----------------------------------

- If you already have 1D node families (Lobatto-Gauss-Legendre nodes are_
  readily_ available_ in_ many_ frameworks_), the recursive rule in 
  the documentation of :func:`recursive_nodes` is incredibly simple to implement.
  This is how they are implemented in this package:

.. literalinclude:: ../../recursivenodes/nodes.py
   :pyobject: _recursive

- They have full `S_{d+1}` symmetry in every dimension `d`.

- The boundary traces of `d`-simplex recursive nodes are `(d-1)`-simplex
  recursive nodes, and in the base case the edges always match the 1D node set
  used in construction.

- When built from the Lobatto-Gauss-Legendre family, they perform about as well
  in 2D as other explicitly constructed node families, and much better in 3D.

- Until optimal nodes are computed for the tetrahedron, they have the best
  known Lebesgue constants in 3D for `n \geq 7`.


.. _are: https://github.com/JuliaApproximation/FastGaussQuadrature.jl
.. _readily: https://github.com/nschloe/quadpy/blob/master/quadpy/line_segment/_gauss_lobatto.py
.. _available: https://www.mathworks.com/matlabcentral/fileexchange/4775-legende-gauss-lobatto-nodes-and-weights
.. _in: https://people.sc.fsu.edu/~jburkardt/cpp_src/quadrule/quadrule.html
.. _many: https://people.sc.fsu.edu/~jburkardt/c_src/quadrule/quadrule.html
.. _frameworks: http://www.netlib.no/netlib/go/gaussq.f

-----------------
How does it work?
-----------------

Equispaced triangle nodes project onto 1D equispaced nodes on the edges of the triangle:


.. tikz::

   \definecolor{tabblue}{rgb}{0.1216,0.4667,0.7059}
   \definecolor{taborange}{rgb}{1.,0.4980,0.0549}
   \definecolor{tabgreen}{rgb}{0.1725,0.6275,0.1725}
   \begin{scope}[scale=3.0]
   \draw[ultra thick] (-1,-.577) -- (1.,-.577) -- (0, 1.15) -- cycle;
   \path (-1,-.577) node [below left] {$(0,0,\textcolor{tabgreen}{1})$};
   \path (1,-.577) node [below right] {$(\textcolor{tabblue}{1},0,0)$};
   \path (0,1.15) node [above] {$(0,\textcolor{taborange}{1},0)$};
   \path (-1,-.577) node[rectangle, inner sep=0pt, minimum width=4pt, minimum height=4pt, fill={tabgreen}, draw=none] (b0) {};
   \path (1,-.577) node[rectangle, inner sep=0pt, minimum width=4pt, minimum height=4pt, fill={tabblue}, draw=none] (b4) {};
   \path (0,1.15) node[rectangle, inner sep=0pt, minimum width=4pt, minimum height=4pt, fill={taborange}, draw=none] (b7) {};
   \path (-.5,-.577) node[circle, inner sep=0pt, minimum width=6pt, fill={taborange}, draw=none] (b1) {}
         (0.,-.577) node[circle, inner sep=0pt, minimum width=4pt, fill={taborange!50!}, draw=none] (b2) {}
         (.5,-.577) node[circle, inner sep=0pt, minimum width=4pt, fill={taborange!50!}, draw=none] (b3) {};
   \path (.667,0.) node[circle, inner sep=0pt, minimum width=4pt, fill={tabgreen!50!}, draw=none] (b5) {}
         (.333,0.577) node[circle, inner sep=0pt, minimum width=6pt, fill={tabgreen}, draw=none] (b6) {};
   \path (-.2,.808) node[circle, inner sep=0pt, minimum width=4pt, fill={tabblue!50!}, draw=none] (b8) {}
         (-.4,.462) node[circle, inner sep=0pt, minimum width=4pt, fill={tabblue!50!}, draw=none] (b9) {}
         (-.6,.115) node[circle, inner sep=0pt, minimum width=6pt, fill={tabblue}, draw=none] (b10) {}
         (-.8,-.231) node[circle, inner sep=0pt, minimum width=4pt, fill={tabblue!50!}, draw=none] (b11) {};
   \draw [densely dashed,tabblue!50!gray] (b4) -- (b10);
   \draw [densely dashed,tabgreen!50!gray] (b0) -- (b6);
   \draw [densely dashed,taborange!50!gray] (b7) -- (b1);
   \path (b1) node[below] {$(\textcolor{tabblue}{1}/4,0,\textcolor{tabgreen}{3}/4)$};
   \path (b6) node[above right] {$(\textcolor{tabblue}{1}/3,\textcolor{taborange}{2}/3,0)$};
   \path (b10) node[above left] {$(0,\textcolor{taborange}{2}/5,\textcolor{tabgreen}{3}/5)$};
   \path (-0.333,0) node[right] {$(\textcolor{tabblue}{1}/6,\textcolor{taborange}{2}/6,\textcolor{tabgreen}{3}/6)$};
   \path (-0.333,0) node[circle, inner sep=0pt, minimum width=2pt, fill={black}, draw=none] (v) {};
   \end{scope}

Blyth and Pozrikidis observed in :cite:`BlPo06` that `Fekete points`_ of the
triangle, which for low degrees have good Lebesgue constants, nearly project onto
Lobatto-Gauss-Legendre nodes on the edges of the triangle:

.. pull-quote::

   Some intriguing observations can be made regarding the location of some of
   the Fekete points in a given set. [...] If an imaginary line is drawn
   through the nodes [...] the two Fekete nodes sit on this line, close to the
   two zeros of the second Lobatto polynomial, Lo2, scaled by the length of the
   imaginary line.

If we try to reverse engineer a point that projects onto Lobatto-Gauss-Legendre nodes,
the lines nearly meet, but not quite:

.. tikz::

   \definecolor{tabblue}{rgb}{0.1216,0.4667,0.7059}
   \definecolor{taborange}{rgb}{1.,0.4980,0.0549}
   \definecolor{tabgreen}{rgb}{0.1725,0.6275,0.1725}
   \begin{scope}[scale=3.0]
   \draw[ultra thick] (-1,-.577) -- (1.,-.577) -- (0, 1.15) -- cycle;
   \path (-1,-.577) node [below left] {$(0,0,\textcolor{tabgreen}{1})$};
   \path (1,-.577) node [below right] {$(\textcolor{tabblue}{1},0,0)$};
   \path (0,1.15) node [above] {$(0,\textcolor{taborange}{1},0)$};
   \path (-1,-.577) node[rectangle, inner sep=0pt, minimum width=4pt, minimum height=4pt, fill={tabgreen}, draw=none] (b0) {};
   \path (-.655,-.577) node[circle, inner sep=0pt, minimum width=6pt, fill={taborange}, draw=none] (b1) {}
         (0.,-.577) node[circle, inner sep=0pt, minimum width=4pt, fill={taborange!50!}, draw=none] (b2) {}
         (.655,-.577) node[circle, inner sep=0pt, minimum width=4pt, fill={taborange!50!}, draw=none] (b3) {};
   \path (1,-.577) node[rectangle, inner sep=0pt, minimum width=4pt, minimum height=4pt, fill={tabblue}, draw=none] (b4) {};
   \path (.724,-.1) node[circle, inner sep=0pt, minimum width=4pt, fill={tabgreen!50!}, draw=none] (b5) {}
         (.276,.673) node[circle, inner sep=0pt, minimum width=6pt, fill={tabgreen}, draw=none] (b6) {};
   \path (0,1.15) node[rectangle, inner sep=0pt, minimum width=4pt, minimum height=4pt, fill={taborange}, draw=none] (b7) {};
   \path (-.117,.947) node[circle, inner sep=0pt, minimum width=4pt, fill={tabblue!50!}, draw=none] (b8) {}
         (-.357,.533) node[circle, inner sep=0pt, minimum width=4pt, fill={tabblue!50!}, draw=none] (b9) {}
         (-.643,.040) node[circle, inner sep=0pt, minimum width=6pt, fill={tabblue}, draw=none] (b10) {}
         (-.883,-.374) node[circle, inner sep=0pt, minimum width=4pt, fill={tabblue!50!}, draw=none] (b11) {};
   \draw [densely dashed,tabblue!50!gray] (b4) -- (b10);
   \draw [densely dashed,tabgreen!50!gray] (b0) -- (b6);
   \draw [densely dashed,taborange!50!gray] (b7) -- (b1);
   \path (b1) node[below] {$(x_{4,\textcolor{tabblue}{1}},0,x_{4,\textcolor{tabgreen}{3}})$};
   \path (b6) node[above right] {$(x_{3,\textcolor{tabblue}{1}},x_{3,\textcolor{taborange}{2}},0)$};
   \path (b10) node[above left] {$(0,x_{5,\textcolor{taborange}{2}},x_{5,\textcolor{tabgreen}{3}})$};
   \end{scope}

Going back to equispaced nodes, we find that the intersection point of the
three projection lines can be written in barycentric coordinates *relative to
the projection points*, and that these relative barycentric coordinates are themselves
points from a 1D equispaced node set:

.. tikz::

   \definecolor{tabblue}{rgb}{0.1216,0.4667,0.7059}
   \definecolor{taborange}{rgb}{1.,0.4980,0.0549}
   \definecolor{tabgreen}{rgb}{0.1725,0.6275,0.1725}
   \begin{scope}[scale=3.0]
   \path (-1,-.577) node [below left] {$(0,0,\textcolor{tabgreen}{1})$};
   \path (1,-.577) node [below right] {$(\textcolor{tabblue}{1},0,0)$};
   \path (0,1.15) node [above] {$(0,\textcolor{taborange}{1},0)$};
   \draw [fill=tabblue!50!,draw=none] (-.5,-.577) -- (.333,0.577) -- (-0.333,0) -- cycle;
   \draw [fill=taborange!50!,draw=none] (-.6,.115) -- (.333,0.577) -- (-0.333,0) -- cycle;
   \draw [fill=tabgreen!50!,draw=none] (-.6,.115) -- (-.5,-.577) -- (-0.333,0) -- cycle;
   \draw[ultra thick] (-1,-.577) -- (1.,-.577) -- (0, 1.15) -- cycle;
   \path (-.5,-.577) node[circle, inner sep=0pt, minimum width=6pt, fill={taborange}, draw=none] (b1) {};
   \path (.333,0.577) node[circle, inner sep=0pt, minimum width=6pt, fill={tabgreen}, draw=none] (b6) {};
   \path (-.6,.115) node[circle, inner sep=0pt, minimum width=6pt, fill={tabblue}, draw=none] (b10) {};
   \path (-0.333,0) node[circle, inner sep=0pt, minimum width=2pt, fill={black}, draw=none] (v) {};
   \path (b1) node[below] {$(\textcolor{tabblue}{1}/4,0,\textcolor{tabgreen}{3}/4)$};
   \path (b6) node[above right] {$(\textcolor{tabblue}{1}/3,\textcolor{taborange}{2}/3,0)$};
   \path (b10) node[above left] {$(0,\textcolor{taborange}{2}/5,\textcolor{tabgreen}{3}/5)$};
   \path (0.278,-0.192) node[align=right] {$\textcolor{tabblue}{(1-1/6)}$\\$:\textcolor{taborange}{(1-2/6)}$\\$:\textcolor{tabgreen}{(1-3/6)}$};
   \end{scope}

(Note that the relative barycentric coordinates have not been scaled to sum to 1.)

Using the same idea by analogy with Lobatto-Gauss-Legendre points, we have the
recursive node definition:

.. tikz::

   \definecolor{tabblue}{rgb}{0.1216,0.4667,0.7059}
   \definecolor{taborange}{rgb}{1.,0.4980,0.0549}
   \definecolor{tabgreen}{rgb}{0.1725,0.6275,0.1725}
   \begin{scope}[scale=3.0]
   \draw[ultra thick] (-1,-.577) -- (1.,-.577) -- (0, 1.15) -- cycle;
   \path (-1,-.577) node [below left] {$(0,0,\textcolor{tabgreen}{1})$};
   \path (1,-.577) node [below right] {$(\textcolor{tabblue}{1},0,0)$};
   \path (0,1.15) node [above] {$(0,\textcolor{taborange}{1},0)$};
   \draw [fill=tabblue!50!,draw=none] (-.655,-.577) -- (.276,0.673) -- (-0.433,-.023) -- cycle;
   \draw [fill=taborange!50!,draw=none] (-.643,.040) -- (.276,0.673) -- (-0.433,-.023) -- cycle;
   \draw [fill=tabgreen!50!,draw=none] (-.643,.040) -- (-.655,-.577) -- (-0.433,-.023) -- cycle;
   \path (-.655,-.577) node[circle, inner sep=0pt, minimum width=6pt, fill={taborange}, draw=none] (b1) {};
   \path (.276,.673) node[circle, inner sep=0pt, minimum width=6pt, fill={tabgreen}, draw=none] (b6) {};
   \path (-.643,.040) node[circle, inner sep=0pt, minimum width=6pt, fill={tabblue}, draw=none] (b10) {};
   \path (-0.433,-0.023) node[circle, inner sep=0pt, minimum width=2pt, fill={black}, draw=none] (v) {};
   \path (-0.433,-0.023) node[right,xshift=0.7cm, yshift=0.1cm]  {$\boldsymbol{b}_{\boldsymbol{X}}((\textcolor{tabblue}{1},\textcolor{taborange}{2},\textcolor{tabgreen}{3}))$};
   \path (b1) node[below] {$(x_{4,\textcolor{tabblue}{1}},0,x_{4,\textcolor{tabgreen}{3}})$};
   \path (b6) node[above right] {$(x_{3,\textcolor{tabblue}{1}},x_{3,\textcolor{taborange}{2}},0)$};
   \path (b10) node[above left] {$(0,x_{5,\textcolor{taborange}{2}},x_{5,\textcolor{tabgreen}{3}})$};
   \path (0.207,-0.160) node[align=right,yshift=-0.5cm] {$\textcolor{tabblue}{x_{6,6-1}}$\\$:\textcolor{taborange}{x_{6,6-2}}$\\$:\textcolor{tabgreen}{x_{6,6-3}}$};
   \end{scope}

Or, put into the form given in the definition:

.. tikz::

   \definecolor{tabblue}{rgb}{0.1216,0.4667,0.7059}
   \definecolor{taborange}{rgb}{1.,0.4980,0.0549}
   \definecolor{tabgreen}{rgb}{0.1725,0.6275,0.1725}
   \begin{scope}[scale=3.0]
   \draw[ultra thick] (-1,-.577) -- (1.,-.577) -- (0, 1.15) -- cycle;
   \path (-1,-.577) node [below left] {$(0,0,\textcolor{tabgreen}{1})$};
   \path (1,-.577) node [below right] {$(\textcolor{tabblue}{1},0,0)$};
   \path (0,1.15) node [above] {$(0,\textcolor{taborange}{1},0)$};
   \draw [fill=tabblue!50!,draw=none] (-.655,-.577) -- (.276,0.673) -- (-0.433,-.023) -- cycle;
   \draw [fill=taborange!50!,draw=none] (-.643,.040) -- (.276,0.673) -- (-0.433,-.023) -- cycle;
   \draw [fill=tabgreen!50!,draw=none] (-.643,.040) -- (-.655,-.577) -- (-0.433,-.023) -- cycle;
   \path (-.655,-.577) node[circle, inner sep=0pt, minimum width=6pt, fill={taborange}, draw=none] (b1) {};
   \path (.276,.673) node[circle, inner sep=0pt, minimum width=6pt, fill={tabgreen}, draw=none] (b6) {};
   \path (-.643,.040) node[circle, inner sep=0pt, minimum width=6pt, fill={tabblue}, draw=none] (b10) {};
   \path (-0.433,-0.023) node[circle, inner sep=0pt, minimum width=2pt, fill={black}, draw=none] (v) {};
   \path (-0.433,-0.023) node[right, xshift=0.7cm, yshift=0.1cm] {$\boldsymbol{b}_{\boldsymbol{X}}(\boldsymbol{\alpha})$};
   \path (b1) node[below] {$\boldsymbol{b}_{\boldsymbol{X}}(\textcolor{taborange}{\boldsymbol{\alpha}_{\backslash 1}})_{+1}$};
   \path (b6) node[above right] {$\boldsymbol{b}_{\boldsymbol{X}}(\textcolor{tabgreen}{\boldsymbol{\alpha}_{\backslash 2}})_{+2}$};
   \path (b10) node[above left] {$\boldsymbol{b}_{\boldsymbol{X}}(\textcolor{tabblue}{\boldsymbol{\alpha}_{\backslash 0}})_{+0}$};
   \path (0.207,-0.160) node[align=right,yshift=-0.5cm] {$\textcolor{tabblue}{x_{|\boldsymbol{\alpha}|,|\boldsymbol{\alpha}_{\backslash 0}|}}$\\$:\textcolor{taborange}{x_{|\boldsymbol{\alpha}|,|\boldsymbol{\alpha}_{\backslash 1}|}}$\\$:\textcolor{tabgreen}{x_{|\boldsymbol{\alpha}|,|\boldsymbol{\alpha}_{\backslash 2}|}}$};
   \end{scope}

---------------------------
Where is there more detail?
---------------------------

A preprint has been submitted to arXiv_ :cite:`Isaa20b`.  You can read a version
that includes source code for all tables and figures here_ :cite:`Isaa20`.

.. _arxiv: https://arxiv.org/abs/2002.09421
.. _here: ./_static/recursivenodes.pdf


.. _Fekete points: https://en.wikipedia.org/wiki/Fekete_problem

