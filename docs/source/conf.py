# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'recursivenodes'
copyright = '2020, Toby Isaac'
author = 'Toby Isaac'
gitlab_url = 'https://gitlab.com/tisaac/recursivenodes/'

# The full version, including alpha/beta/rc tags
import os
import codecs
import re

here = os.path.abspath(os.path.dirname(__file__))

def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()

def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

release = find_version('..','..','recursivenodes','__init__.py')


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.mathjax',
    'sphinx.ext.napoleon',
    'matplotlib.sphinxext.plot_directive',
    'sphinxcontrib.bibtex',
    'sphinxcontrib.tikz',
    'sphinx.ext.viewcode',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

html_theme_options = {
        'sidebar_width': '230px',
        'logo': 'recursivenodes.png',
        'logo_name': 'true',
        'description': 'simple nodes for simplices',
        'fixed_sidebar': 'true',
        }

html_sidebars = {
    "**": [
        "about.html",
        "gitlab.html",
        "doi.html",
        "navigation.html",
        "relations.html",
        "searchbox.html",
    ]
}

html_context = {
        'gitlab_user': 'tisaac',
        'gitlab_repo': 'recursivenodes',
        'gitlab_branch': 'master',
        'gitlab_pipeline': 'true',
        'gitlab_coverage': 'true',
        'doi_zenodo': '10.5281/zenodo.3675431',
        }

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Extension configuration -------------------------------------------------

# Turn off links above matplotlib figures
plot_html_show_source_link = False
plot_html_show_formats = False
napoleon_use_admonition_for_notes = True

